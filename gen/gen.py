# Generate all trig tests from a template

from glob import glob
from math import (sin, cos, tan, sinh, cosh, tanh, asin, acos, atan, log, exp,
        sqrt)
import os
import shutil

from jinja2 import Template

trig = [{
    "fn": "sin",
    "value": str(sin(1)),
}, {
    "fn": "cos",
    "value": str(cos(1)),
}, {
    "fn": "tan",
    "value": str(tan(1)),
}, {
    "fn": "sinh",
    "value": str(sinh(1)),
}, {
    "fn": "cosh",
    "value": str(cosh(1)),
}, {
    "fn": "tanh",
    "value": str(tanh(1)),
}, {
    "fn": "asin",
    "value": str(asin(1)),
}, {
    "fn": "acos",
    "value": str(acos(1)),
}, {
    "fn": "atan",
    "value": str(atan(1)),
}, {
    "fn": "exp",
    "value": str(exp(1)),
}, {
    "fn": "log",
    "value": str(log(1)),
}, {
    "fn": "sqrt",
    "value": str(sqrt(1)),
}]

base_path = "../tests/intrinsic"

for fn in trig:
    base_dir = base_path + "/" + fn["fn"]
    if os.path.exists(base_dir):
        shutil.rmtree(base_dir)
    os.mkdir(base_dir)
    for filename in glob("trig/*.f90"):
        template = Template(open(filename).read())
        s = template.render(fn)
        nfilename = base_dir + "/" + os.path.basename(filename)
        print(nfilename)
        open(nfilename, "w").write(s)
