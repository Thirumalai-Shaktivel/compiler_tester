! basic derived types
program t01_derived_type
implicit none
type type_A
    integer :: i
    real :: r
end type
type(type_A) :: A
A%i = 5
A%r = 5.5
end program
