! single/double complex
program complex_kind
implicit none
integer, parameter :: sp = kind(0.0), dp = kind(0.d0)
complex(sp) :: a
complex(dp) :: b
a = 0
a = 1
a = 3.14
a = 3.14_sp
a = 3.14_dp

a = (3.14, 2.14)
a = (3.14_sp, 2._sp)
a = (3.14_dp, 3._dp)

b = 0
b = 1
b = 3.14
b = 3.14_sp
b = 3.14_dp

b = (3.14, 2.14)
b = (3.14_sp, 2._sp)
b = (3.14_dp, 3._dp)
end program
