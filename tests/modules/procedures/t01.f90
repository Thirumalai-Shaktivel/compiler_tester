! Module functions
module A
implicit none

contains

    integer function f(x)
    integer, intent(in) :: x
    f = x + 1
    end function

end module

module B
use A, only: f
implicit none
end module

program t01
use B, only: f
implicit none
integer :: i
i = f(5)
print *, i
end program
