! array 1D single/double real
program t03_array1d_real
implicit none
integer, parameter :: sp = kind(0.0), dp = kind(0.d0)
integer, parameter :: n = 10
real(sp) :: x4(n)
real(dp) :: x8(n)
x4 = 3.14_sp
x4 = abs(x4)

x8 = 3.14_dp
x8 = abs(x8)
end program
