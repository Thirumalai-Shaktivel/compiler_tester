! scalar single/double complex
program t02_scalar_complex
implicit none
integer, parameter :: sp = kind(0.0), dp = kind(0.d0)
complex(sp) :: x4
complex(dp) :: x8
x4 = (3.14_sp, 3._sp)
x4 = atan(x4)
x4 = atan((3.14_sp, 3._sp))

x8 = (3.14_dp, 3._dp)
x8 = atan(x8)
x8 = atan((3.14_dp, 3._dp))
end program