! scalar single/double real
program t01_scalar_real
implicit none
integer, parameter :: sp = kind(0.0), dp = kind(0.d0)
real(sp) :: x4, x4_ref
real(dp) :: x8, x8_ref
x4 = 1._sp
x4 = mod(x4, 3._sp)
x4 = mod(6.5_sp, 2._sp)
x4_ref = 0.5_sp
if (abs(x4 - x4_ref) > 1e-5_sp) error stop "mod(6.5_sp, 2._sp)"

x8 = 1._dp
x8 = mod(x4, 3._dp)
x8 = mod(6.5_dp, 2._dp)
x8_ref = 0.5_dp
if (abs(x8 - x8_ref) > 1e-10_dp) error stop "mod(6.5_dp, 2._dp)"
end program
