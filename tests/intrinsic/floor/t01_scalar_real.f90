! scalar single/double real
program t01_scalar_real
implicit none
integer, parameter :: sp = kind(0.0), dp = kind(0.d0)
integer :: r
real(sp) :: x4
real(dp) :: x8
x4 = 1._sp
r = floor(x4)
r = floor(6.5_sp)
if (r /= 6) error stop "floor(6.5_sp)"

x8 = 1._dp
r = floor(x4)
r = floor(6.5_dp)
if (r /= 6) error stop "floor(6.5_dp)"
end program
