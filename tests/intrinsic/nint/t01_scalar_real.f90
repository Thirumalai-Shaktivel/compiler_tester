! scalar single/double real
program t01_scalar_real
implicit none
integer, parameter :: sp = kind(0.0), dp = kind(0.d0)
integer :: r
real(sp) :: x4
real(dp) :: x8
x4 = 1._sp
r = nint(x4)
r = nint(6.7_sp)
if (r /= 7) error stop "nint(6.7_sp)"

x8 = 1._dp
r = nint(x4)
r = nint(6.7_dp)
if (r /= 7) error stop "nint(6.7_dp)"
end program
