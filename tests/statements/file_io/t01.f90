! open/read/write/close
program t01
implicit none
integer :: a, b
integer :: u

a = 4
b = 5

open(newunit=u, file="log2.txt", status="replace")
write(u, *) a, b
close(u)

a = 0
b = 0

open(newunit=u, file="log2.txt", status="old")
read(u, *) a, b
close(u)

if (a /= 4) error stop "a"
if (b /= 5) error stop "b"

end
